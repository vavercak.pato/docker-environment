# docker-environment tools

This repository targets on docker images that should be able to easily build all projects I use for my diploma work.

## For those not familiar with Docker
### Build an image
```bash
cd <directory_with_dockerfile>
docker build -t <custom_docker_image_name> .
```
For example, if you decide to use ubuntu docker, do this:
```bash
cd ubuntu
docker build -t my_ubuntu_image:1.0.0 .
```
### Run docker container
example:
```bash
docker run -ti -d -u root --name my_container --entrypoint cat my_ubuntu_image:1.0.0
```
### Attach to a running container
example:
```bash
docker exec -ti my_container bash
```
